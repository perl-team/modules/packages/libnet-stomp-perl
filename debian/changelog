libnet-stomp-perl (0.62-1) unstable; urgency=medium

  * Team upload.
  * Import upstream version 0.62.
  * Declare compliance with Debian Policy 4.6.2.

 -- gregor herrmann <gregoa@debian.org>  Sat, 24 Feb 2024 17:56:32 +0100

libnet-stomp-perl (0.61-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 16 Oct 2022 05:52:56 +0100

libnet-stomp-perl (0.61-1) unstable; urgency=medium

  * Import upstream version 0.61.
  * Update years of packaging copyright
  * Bump dh compat to level 13
  * Add Rules-Requires-Root: no
  * Declare compliance with Debian Policy 4.6.0

 -- Florian Schlichting <fsfs@debian.org>  Wed, 01 Sep 2021 22:12:49 +0800

libnet-stomp-perl (0.60-1) unstable; urgency=medium

  * Team upload.

  [ gregor herrmann ]
  * Remove Sebastien Aperghis-Tramoni from Uploaders.
    Thanks for your work!

  [ Alex Muntada ]
  * Remove inactive pkg-perl members from Uploaders.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * Import upstream version 0.60.
  * Add new build and runtime dependencies.
  * Annotate test-only build dependencies with <!nocheck>.
  * Declare compliance with Debian Policy 4.4.1.
  * Drop unneeded version constraints from (build) dependencies.
  * Bump debhelper-compat to 12.
  * debian/watch: use uscan version 4.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata.
  * Update link in long description. Thanks to lintian.

 -- gregor herrmann <gregoa@debian.org>  Fri, 27 Dec 2019 05:00:18 +0100

libnet-stomp-perl (0.57-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Add debian/upstream/metadata
  * Import upstream version 0.57
  * Upstream update contact.
  * Declare compliance with Debian Policy 3.9.8.
  * Drop unneeded version constraints.
  * Bump debhelper compatibility level to 9.

 -- gregor herrmann <gregoa@debian.org>  Sat, 02 Jul 2016 22:47:50 +0200

libnet-stomp-perl (0.56-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Thu, 09 Jul 2015 21:43:36 +0200

libnet-stomp-perl (0.55-1) unstable; urgency=medium

  * Team upload.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.
  * New upstream release.
  * Declare compliance with Debian Policy 3.9.6.
  * Mark package as autopkgtest-able.

 -- gregor herrmann <gregoa@debian.org>  Sat, 30 May 2015 20:09:50 +0200

libnet-stomp-perl (0.49-1) unstable; urgency=medium

  * Team upload.

  [ Florian Schlichting ]
  * Import Upstream version 0.48

  [ gregor herrmann ]
  * New upstream release 0.49.
  * Add alternative for IO::Socket::IP BD/Recommends.

 -- gregor herrmann <gregoa@debian.org>  Sat, 19 Jul 2014 23:18:01 +0200

libnet-stomp-perl (0.47-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Florian Schlichting ]
  * Import Upstream version 0.47
  * Raise Module::Build dependency to 0.42
  * Add build-dependencies on Test::Deep and Test::Fatal

 -- Florian Schlichting <fsfs@debian.org>  Sat, 12 Jul 2014 09:49:49 +0200

libnet-stomp-perl (0.46-1) unstable; urgency=low

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Florian Schlichting ]
  * Import Upstream version 0.46
  * Email change: Florian Schlichting -> fsfs@debian.org
  * Recommend modules required for IPv6 support
  * Update years of packaging copyright
  * Declare compliance with Debian Policy 3.9.5

 -- Florian Schlichting <fsfs@debian.org>  Mon, 20 Jan 2014 21:19:16 +0100

libnet-stomp-perl (0.45-1) unstable; urgency=low

  * Imported Upstream version 0.45.
  * Bumped Standards-Version to 3.9.3 (use copyright-format 1.0).
  * Added myself to debian/copyright.

 -- Florian Schlichting <fschlich@zedat.fu-berlin.de>  Thu, 15 Mar 2012 22:03:31 +0100

libnet-stomp-perl (0.44-1) unstable; urgency=low

  [ gregor herrmann ]
  * Remove debian/source/local-options; abort-on-upstream-changes and
    unapply-patches are default in dpkg-source since 1.16.1.

  [ Florian Schlichting ]
  * Imported Upstream version 0.44
  * Added myself to Uploaders.

 -- Florian Schlichting <fschlich@zedat.fu-berlin.de>  Fri, 25 Nov 2011 21:32:36 +0000

libnet-stomp-perl (0.42-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * New upstream release.
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

 -- Ansgar Burchardt <ansgar@debian.org>  Sun, 21 Aug 2011 13:53:10 +0200

libnet-stomp-perl (0.41-1) unstable; urgency=low

  * New upstream release.
  * Bump Standards-Version to 3.9.2 (no changes).

 -- Ansgar Burchardt <ansgar@debian.org>  Sat, 14 May 2011 16:02:55 +0200

libnet-stomp-perl (0.40-1) unstable; urgency=low

  * New upstream release.

 -- Ansgar Burchardt <ansgar@debian.org>  Sat, 05 Mar 2011 14:09:05 +0100

libnet-stomp-perl (0.39-1) unstable; urgency=low

  * New upstream release.
  * Use debhelper compat level 8.
  * debian/copyright: Add new copyright holder; refer to "Debian systems"
    instead of "Debian GNU/Linux systems".
  * Update my email address.

 -- Ansgar Burchardt <ansgar@debian.org>  Thu, 27 Jan 2011 15:04:22 +0100

libnet-stomp-perl (0.38-1) unstable; urgency=low

  * New upstream release.
  * Remove build-dep on perl (>= 5.10) | libmodule-build-perl:
    stable has perl 5.10.
  * Bump build-dep on debhelper to >= 7.3.7 for --buildsystem option.
  * debian/copyright: Refer to /usr/share/common-licenses/GPL-1.
  * Bump Standards-Version to 3.9.1.

 -- Ansgar Burchardt <ansgar@43-1.org>  Wed, 04 Aug 2010 16:52:21 +0900

libnet-stomp-perl (0.37-1) unstable; urgency=low

  * New upstream release.
  * debian/rules: Fix sed command to insert correct hashbang into examples.
  * Bump Standards-Version to 3.9.0 (no changes).

 -- Ansgar Burchardt <ansgar@43-1.org>  Sat, 10 Jul 2010 21:31:30 +0900

libnet-stomp-perl (0.36-1) unstable; urgency=low

  * New upstream release.

 -- Ansgar Burchardt <ansgar@43-1.org>  Mon, 31 May 2010 13:54:25 +0900

libnet-stomp-perl (0.35-1) unstable; urgency=low

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Salvatore Bonaccorso ]
  * debian/control: Changed: Replace versioned (build-)dependency on
    perl (>= 5.6.0-{12,16}) with an unversioned dependency on perl (as
    permitted by Debian Policy 3.8.3).

  [ Ansgar Burchardt ]
  * New upstream release.
  * Install examples.
  * Use tiny debian/rules.
  * debian/rules: Use Build.PL.
  * Use source format 3.0 (quilt).
  * debian/control: Mention "Perl module" in short description.
  * debian/control: Add perl (>= 5.10) as a preferred alternative to
    libmodule-build-perl build-dep.
  * debian/control: Add (build-)dep on libclass-accessor-perl.
  * debian/control: Add build-dep on libtest-pod-perl and
    libtest-pod-coverage-perl.
  * debian/copyright: Formatting changes for current DEP-5 proposal.
  * debian/copyright: Add additional copyright holders and years of copyright.
  * Bump Standards-Version to 3.8.4.
  * Add myself to Uploaders.

 -- Ansgar Burchardt <ansgar@43-1.org>  Thu, 27 May 2010 00:51:33 +0900

libnet-stomp-perl (0.34-1) unstable; urgency=low

  [ Krzysztof Krzyżaniak (eloy) ]
  * convert package to debhelper7, update to Standards-Version: 3.8.1

  [ Sebastien Aperghis-Tramoni ]
  * Initial release (closes: #510063).

 -- Sebastien Aperghis-Tramoni <sebastien@aperghis.net>  Fri,  5 Dec 2008 14:30:00 +0100
